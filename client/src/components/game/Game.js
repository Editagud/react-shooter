import React from 'react';
import Cookie from 'js-cookie';
import { MESSAGE_TYPES, TARGET_POSITIONS } from '../../shared';
import './Game.css';
import Modal from '../elements/elements';
import history from '../history';

export default function Game({ user }) {
  const [target, setTarget] = React.useState();
  const [joke, setJoke] = React.useState(null);
  const [scoreCount, setScoreCount] = React.useState(0);
  const [open, setIsOpen] = React.useState(false);
  const [totalScoreCount, setTotalScoreCount] = React.useState(null);
  const [timeLeft, setTimeLeft] = React.useState(120);
  const [log, setLog] = React.useState([]);

  React.useEffect(() => {
    // exit early when we reach 0, stop when there is no more time
    if (!timeLeft) {
      setIsOpen(true);
      fetch(`http://localhost:8080/users/score/${user}`, {
        method: 'get',
      }).then((res) => {
        if (res.status === 200) {
          res.json().then((body) => {
            setTotalScoreCount(body.points);
          });
        } else {
          res.status(400).json({ message: 'The user is not found' });
        }
      });
    }

    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);

    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft, open, user]);



  const time = new Date(null);
  time.setSeconds(timeLeft);
  const minutes = time.getMinutes();
  const seconds = time.getSeconds();
  // SOCKETS
  // make ws and interval reference
  const ws = React.useRef();
  //

  React.useEffect(() => {
    // create new socket and sending to server the token in query for verifying
    const token = Cookie.get('token');
    // create websocket, with the token we connect the application to the websockets and we check what user it is
    // we give temporary token to the client- to check the user is validated with login
    ws.current = new WebSocket(`ws://localhost:8080?token=${token}`);
    // google it
    ws.current.onopen = function () { };
  }, []);



  React.useEffect(() => {
    // for the websocket I have now
    // everytime the frontend is getting message and onmessage
    ws.current.onmessage = function (e) {
      const data = JSON.parse(e.data);
      // switch case, do shit for each type
      switch (data.type) {
        case MESSAGE_TYPES.LOG:

          setLog((newLog) => newLog.concat(data.payload));

          break;
        case MESSAGE_TYPES.TARGET_POSITION:
          // every 2 sec moves the target
          setTarget({
            yPos: data.payload.yPos,
            xPos: data.payload.xPos,
          });
          break;
        case MESSAGE_TYPES.JOKE:
          // get the joke from backend
          setJoke(data.payload);
          break;
        default:
          return false;
      }
    };
  }, []);


  return (
    <div className="timer">
      {target && (
        <img
          className="targetStyle"
          src={require('../../imgs/duck.png')}
          // className="targetStyle"
          style={{
            // setting up the initial position of target
            left: TARGET_POSITIONS.y.LEFT === target.yPos ? '5%' : '90%',
            top: `${target.xPos}%`,
          }}
          onClick={function () {
            // Fire a shot to the server in the websocket who shot
            ws.current.send(
              JSON.stringify({

                type: MESSAGE_TYPES.SHOT,
              }),
            );
            setScoreCount(scoreCount + 20);
          }}
        />
      )}
      {joke !== null && (
        <div className="joke-wrapper">
          <h2>{joke.joke}</h2>
          <h2>{joke.setup}</h2>

          {joke.delivery && <p>{joke.delivery}</p>}
        </div>
      )}
      <div
        className="joke"
        style={{

        }}
      >
        <h1>
          {minutes}
          :
          {seconds}
        </h1>
      </div>
      <div
        className="message-log"
      >
        {log.map((message) => <div>{message}</div>)}
      </div>
      <Modal
        isOpen={open}

        onClose={() => {
          history.push('/index');
        }}
      >
        <div className="game-modal">
          <h1>
            Good job
            {user}
            !
          </h1>
          <h2>
            You got:
            {scoreCount}
            {' '}
            points
          </h2>
          <h2>
            In total you have:
            {totalScoreCount}
            {' '}
            points
          </h2>
        </div>
      </Modal>
    </div>
  );
}
