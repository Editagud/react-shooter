import React from 'react';


export default function LandingPage() {
  return (
    <div className="light-background">
      <h1 className="game-title">React shooter</h1>
      <div className="links-wrapper">
        <a href="/login">
          <h1 className="title-spin">Login</h1>
        </a>
        <a href="/signup">
          <h1 className="title-spin">Signup</h1>
        </a>
      </div>
    </div>
  );
}
