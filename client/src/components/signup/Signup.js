import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import history from '../history';

export default function SignupForm() {
    const [values, setValues] = React.useState();
    const [error, setError] = useState();
    async function handleSubmit(e) {
        e.preventDefault();
        const req = await fetch('http://localhost:8080/users/signup', {
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'post',
            body: JSON.stringify(values),
        });
        const response = await req.json();
        if (req.status === 400) {
            setError(response.message);
        } else {
            history.push('/login');
        }
    }
    return (
        <div className="light-background">
            <h1>Sign up</h1>
            <form id="signup-form" onSubmit={handleSubmit} method="POST">
                <div>
                    <TextField id="standard-basic" variant="outlined" label="Username" onChange={(e) => setValues({ ...values, username: e.target.value })} />
                    <TextField id="standard-basic" variant="outlined" label="Email" onChange={(e) => setValues({ ...values, email: e.target.value })} />
                    <TextField id="standard-basic" type="password" variant="outlined" label="Password" onChange={(e) => setValues({ ...values, password: e.target.value })} />
                    <TextField id="standard-basic" type="password" variant="outlined" label="Repeat password" onChange={(e) => setValues({ ...values, repeatPassword: e.target.value })} />
                    <Button variant="contained" type="submit" color="primary">
                        Sign up
                    </Button>
                </div>
            </form>
            <p className="error">{error}</p>
            <p>
                Already have an account?
                <a href="/login">Log in</a>
            </p>
        </div>
    );
}
