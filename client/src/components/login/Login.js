import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import history from '../history';

function LoginForm({ onLogin }) {
  const [values, setValues] = React.useState();
  const [error, setError] = useState();
  async function handleSubmit(e) {
    e.preventDefault();
    const req = await fetch('http://localhost:8080/users/login', {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': '*',
      },
      method: 'post',
      body: JSON.stringify(values),
    });
    const response = await req.json();
    if (req.status === 401) {
      setError(response.message);
    } else {
      onLogin(response.name, response.token);
      history.push('/index');
    }
  }
  return (
    <div className="light-background">
      <h1>Login</h1>
      <form id="login-form" onSubmit={handleSubmit} method="POST">
        <div>
          <TextField
            id="standard-basic"
            variant="outlined"
            label="Username"
            onChange={(e) => setValues({ ...values, username: e.target.value })}
          />
          <TextField
            id="standard-basic"
            type="password"
            variant="outlined"
            label="Password"
            onChange={(e) => setValues({ ...values, password: e.target.value })}
          />
          <Button variant="contained" type="submit" color="primary">
            Log in
          </Button>
        </div>
      </form>
      <p className="error">
        {error}
      </p>
      <p>
        Dont have a user?
        <a href="/signup">Sign up</a>
      </p>
    </div>
  );
}
export default LoginForm;
