import React from 'react';
import history from '../history';

export default function Welcome({ currentUser }) {
    return (
        <div className="light-background">
            <h1 className="game-title">
                Hellloooo,
        {currentUser}
            </h1>
            <div className="links-wrapper">
                <a href="/game">
                    <h1 className="title-spin">Play</h1>
                </a>
                <a
                    onClick={() => {
                        fetch('http://localhost:8080/logout', {
                            method: 'get',
                        }).then((res) => {
                            if (res.status === 200) {
                                history.push('/');
                            }
                        });
                    }}
                >
                    <h1 className="title-spin">Logout</h1>
                </a>
            </div>
        </div>
    );
}
