import React from 'react';

function Logout({ onLogout }) {
  onLogout();
}
export default Logout;
