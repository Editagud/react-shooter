module.exports = {
  //for developer to see what possibilities she has
  MESSAGE_TYPES: {
    SHOT: "SHOT",
    NEW_PLAYER: "NEW_PLAYER",
    TARGET_POSITION: "TARGET_POSITION",
    LOG: "LOG",
    JOKE: "JOKE"
  },
  TARGET_POSITIONS: {
    y: { LEFT: "LEFT", RIGHT: "RIGHT" }
  }
};
