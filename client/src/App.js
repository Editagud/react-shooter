import React from 'react';
import './App.css';
import './style.css';
import { Router, Switch, Route } from 'react-router-dom';
import Cookie from 'js-cookie';
import Landing from './components/landingPage/Landing';
import LoginForm from './components/login/Login';
import SignupForm from './components/signup/Signup';
import Welcome from './components/welcome/Welcome';
import Game from './components/game/Game';
import history from './components/history';

export default function App() {
  const user = Cookie.get('user');
  return (
    <div className="App">
      <Router history={history}>
        <div>
          <Switch>
            <Route exact path="/" component={() => <Landing />} />
            <Route
              exact
              path="/index"
              component={() => <Welcome currentUser={user} />}
            />
            <Route exact path="/game" component={() => <Game user={user} />} />
            <Route
              exact
              path="/login"
              component={() => (
                <LoginForm
                  onLogin={(name, token) => {
                    Cookie.set('token', token);
                    Cookie.set('user', name);
                  }}
                />
              )}
            />
            <Route exact path="/signup" component={() => <SignupForm />} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}
