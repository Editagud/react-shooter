const WebSocket = require('ws');
const express = require('express');

let WSServer = require('ws').Server;
let server = require('http').createServer();
const User = require('./models/User');

const app = express();
// creating random uuid for the target  so we know which user shot
const uuidv4 = require('uuid/v4');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');
// to assign token to each user
const jwt = require('jsonwebtoken');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
// parse application/json
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
const rateLimit = require('express-rate-limit');

const authLimiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 10, // limit each IP to 100 requests per windowMs
});
const Knex = require('knex');
const { Model } = require('objection');

const fetch = require('node-fetch');
const knexConfiq = require('./knexfile');
const config = require('./mysql_config/database');
const { MESSAGE_TYPES, TARGET_POSITIONS } = require('./client/src/shared');


const knex = Knex(knexConfiq.development);
Model.knex(knex);
// ////////////////// websocket///////////////////////
// here we create websocket server


const wss = new WSServer({

  server: server
});
server.on('request', app);
wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log(`received: ${message}`);
    ws.send(JSON.stringify({
      answer: 42
    }));
  });
});

server.listen(8080, function () {

  console.log(`http/ws server listening on 8080`);
});




// const wss = new WebSocket.Server({
//   port: 8080,
// });
// make random position for target
function makeNewTargetXPosition() {
  return (Math.random() * 100).toFixed();
}
// / defined initial position, xPos - x position, yPos - y position
let currentTargetPosition = {
  uuid: uuidv4(),
  xPos: makeNewTargetXPosition(),
  yPos: TARGET_POSITIONS.y.LEFT,
};
// is it's left it goes to the right
function makeNewTargetYPosition() {
  return currentTargetPosition.yPos === TARGET_POSITIONS.y.LEFT
    ? TARGET_POSITIONS.y.RIGHT
    : TARGET_POSITIONS.y.LEFT;
}

makeNewTargetYPosition();

// gives to each client the actual target position
function updateClientsTargetPosition(newPos) {
  wss.clients.forEach((client) => {
    client.send(
      JSON.stringify({
        type: MESSAGE_TYPES.TARGET_POSITION,
        payload: newPos,
      }),
    );
  });
}
// set update every 2 sec
let interval = setInterval(() => {
  const xPos = makeNewTargetXPosition();
  const yPos = makeNewTargetYPosition();
  currentTargetPosition = {
    xPos,
    yPos,
  };
  updateClientsTargetPosition(currentTargetPosition);
}, 2000);

// when someone connects,
wss.on('connection', (ws, request) => {
  let user;

  // check token is valid and decode user
  // here we get the token from url
  const token = request.url.split('=')[1];
  // google it
  // taking tiken and check if it's valid
  jwt.verify(token, config.secret, (err, decoded) => {
    // TODO: handle error redirect to login
    if (err) {
      return false;
    }
    // decoding object from verified data
    user = decoded.data;
    // show new connected user
    wss.clients.forEach((client) => {
      client.send(
        JSON.stringify({
          type: MESSAGE_TYPES.LOG,
          payload: `New player ${user.username}`,
        }),
      );
    });
  });

  if (user) {
    ws.on('message', async (data) => {
      const message = JSON.parse(data);
      async function fetchJoke() {
        const jokeRequest = await fetch(
          'https://sv443.net/jokeapi/category/Programming ',
        );
        const response = await jokeRequest.json();
        return response;
      }
      const joke = await fetchJoke();
      wss.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
          // send to frontend which client clicked the target and stop the target's movement
          switch (message.type) {
            case MESSAGE_TYPES.SHOT:
              client.send(
                JSON.stringify({
                  type: MESSAGE_TYPES.LOG,
                  payload: `Player ${user.username} scored!`,
                }),
              );
              clearInterval(interval);

              async function updateScore(username) {
                const selectedUser = await User.query()
                  .select()
                  .where({ username });
                if (selectedUser.length > 0) {
                  let oldScore = selectedUser[0].points
                  const updatedScroreRow = await User.query().update({ points: oldScore + 20 }).where({ username });
                  if (updatedScroreRow === 1) {
                    console.log('scoreupdated')
                  }

                }
              }
              updateScore(user.username)
              // changing uuid of target after it had been shot
              currentTargetPosition = {
                uuid: uuidv4(),
              };
              // 5 seconds it's nmobing
              setTimeout(() => {
                interval = setInterval(() => {
                  currentTargetPosition = {
                    xPos: makeNewTargetXPosition(),
                    yPos: makeNewTargetYPosition(),
                  };
                  updateClientsTargetPosition(currentTargetPosition);
                }, 1500);
              }, 5000);
              // fetch joke and send to front end

              client.send(
                JSON.stringify({
                  type: MESSAGE_TYPES.JOKE,
                  payload: joke,
                }),
              );
              // end of switch case
              break;
          }
        }
      });
    });
  } else ws.close('You are not logged in');
});
// ///////////////////////////////////////////////////////////////


// store session secret somewhere
// Resave false - so the session doesn't resave every time - we need to store it and update
// saveUninitialized - we don't store anything on user and we dont save anything untill we have something
app.use(
  session({ secret: config.secret, saveUninitialized: true, resave: false }),
);

const usersRoute = require('./routes/usersRoute');

app.use(usersRoute);

app.use('/users/login', session, authLimiter);
app.use('/users/signup', session, authLimiter);

// /// Logout /////
app.get('/logout', (req, res, next) => {
  if (req.session) {
    // delete session object
    req.session.destroy((err) => {
      if (err) {
        return next(err);
      }
      res.status(200).json('session destroyed');
    });
  }
});
