exports.seed = function makeSeeds(knex) {
  // Deletes ALL existing entries
  return knex('users')
    .del()
    .then(() => knex('users').insert([
      {
        username: 'admin',
        email: 'example@example.com',
        password: 'password',
      },
      {
        username: 'vero',
        email: 'vero@vero.com',
        password: 'password',
      },
      {
        username: 'edita',
        email: 'edita.gudan@gmail.com',
        password: 'password',
      },
    ]));
};
