const router = require('express').Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const User = require('../models/User');
require('../mysql_config/passport')(passport);
const config = require('../mysql_config/database');

router.get('/users', async (req, res) => {
  const users = await User.query()
    .select();
  res.json(users);
});

router.post('/users/login', async (req, res) => {
  const { username } = req.body;
  if (!username) {
    res.status(401).json({ message: 'Username is missing' });
    return;
  }
  const { password } = req.body;
  if (!password) {
    res.status(401).json({ message: 'Password is missing' });
    return;
  }

  const validUser = await User.query()
    .select()
    .where({ username });
  if (validUser.length > 0) {
    bcrypt.compare(password, validUser[0].password, (err, response) => {
      if (err) {
        res.status(500).json({ err: 'Problem hashing password' });
        return;
      }
      if (response === true) {
        req.session.user = validUser[0];
        req.session.save();
        // / create token
        const token = jwt.sign({ data: req.session.user }, config.secret);

        res.status(200).json({
          message: 'User logged in',
          name: req.session.user.username,
          token,
        });
      } else {
        res.status(401).json({ message: 'Wrong password' });
      }
    });
  } else {
    res.status(401).json({ message: "User doesn't exist" });
  }
});

router.post('/users/signup', async (req, res) => {
  const { username } = req.body;
  if (!username) {
    res.status(400).json({ message: 'Username is missing' });
    return;
  }
  if (username.length < 4) {
    res.status(400).json({ message: 'Username has to have at least 5 characters' });
    return;
  }
  const { email } = req.body;
  if (!email) {
    res.status(400).json({ message: 'Email is missing' });
    return;
  }
  const { password } = req.body;
  if (!password) {
    res.status(400).json({ message: 'Password is missing' });
    return;
  }
  if (password.length < 5) {
    res.status(400).json({ message: 'Password has to have at least 5 characters' });
    return;
  }


  const { repeatPassword } = req.body;
  if (!repeatPassword) {
    res.status(400).json({ message: 'Repeat password missing' });
    return;
  }
  if (password !== repeatPassword) {
    res.status(400).json({ message: "Passwords don't match" });
    return;
  }
  bcrypt.hash(req.body.password, 10, async (err, hash) => {
    // Store hash in your password DB.

    if (hash) {
      const newUser = {
        username,
        email,
        password: hash,
        points: 0,
      };
      try {
        const user = await User.query().insert(newUser);
        res.status(200).json({ message: user });
      } catch (error) {
        if (error.constraint === 'users_username_unique') {
          res.status(400).json({ message: 'Username is already used' });
        } else if (error.constraint === 'users_email_unique') {
          res.status(400).json({ message: 'Email is already used' });
        } else {
          res.status(400).json({ message: 'There was an error' });
        }
      }
    } else {
      res.status(400).json({ message: 'Problem with password hashing' });
    }
  });
});


router.get('/users/score/:username', async (req, res) => {
  const validUser = await User.query()
    .select()
    .where({ username: req.params.username });
  if (validUser.length > 0) {
    return res.status(200).json({ points: validUser[0].points });
  }
  return res.status(400).json({ message: 'User not found' });
});
module.exports = router;
