exports.up = function (knex) {
  return knex.schema.createTable('users', (table) => {
    table.increments('id');
    table.string('username').unique();
    table.string('email').unique();
    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.string('password');
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('users');
};
